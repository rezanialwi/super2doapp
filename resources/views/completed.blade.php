@extends('layouts.main')

@section('container')
<section class="main">
    @if ($tasks->count() == 0)
    <h3>
        <center><i>Task completed not found</i></center>
    </h3>
    @else


    <div class="dropdown">
        <input id="toggle-all" class="toggle-all" type="checkbox">
        <label for="toggle-all">Mark all as complete</label>
        <div class="dropdown-content">
            <a href="/completedall">Mark all as complete</a>
        </div>
    </div>

    <ul class="todo-list">
        <!-- These are here just to show the structure of the list items -->
        <!-- List items should get the class `editing` when editing and `completed` when marked as completed -->
        @foreach ($tasks as $task)
        <li>
            <div class="view">

                @if ($task->is_complete)

                <input class="toggle" type="checkbox" onclick="changeStatus(event.target, {{ $task->id }});"
                    {{ $task->is_complete ? 'checked' : '' }}>
                <label>
                    <s>{{ $task->title }}</s>
                </label>
                @else
                <input class="toggle" type="checkbox" onclick="changeStatus(event.target, {{ $task->id }});"
                    {{ $task->is_complete ? 'checked' : '' }}>
                <label>
                    {{ $task->title }}
                </label>
                @endif
                {{-- Button delete taskss --}}
                <form action="{{ route('tasks.destroy', $task->id)}}" method="POST" class="d-inline">
                    @method('delete')
                    @csrf
                    <button class="destroy" onclick="return confirm('Are you sure you want to delete ?')">
                    </button>
                </form>
            </div>
        </li>
        @endforeach
    </ul>
    @endif
</section>

<!-- This footer should be hidden by default and shown when there are todos -->
<footer class="footer">
    @if ($tasks->count() == 0)
    <ul class="filters">
        <li>
            <a class="selected" href="/">All</a>
        </li>
    </ul>
    @else
    <!-- This should be `0 items left` by default -->
    <span class="todo-count"><strong>{{$tasks2}}</strong> item left</span>
    <!-- Remove this if you don't implement routing -->
    <ul class="filters">
        <li>
            <a href="/">All</a>
        </li>
        <li>
            <a href="/tasks/active">Active</a>
        </li>
        <li>
            <a class="selected" href="/tasks/completed">Completed</a>
        </li>
    </ul>
    @if ($tasks3->count() == 0)	
        {{-- null --}}
	@else 
    <!-- Hidden if no completed items are left ↓ -->
    <form method="GET" action="{{ route('deleteall') }}">
        @csrf
        <button class="clear-completed">Clear completed</button>
    </form>
	@endif
    @endif
</footer>
@endsection