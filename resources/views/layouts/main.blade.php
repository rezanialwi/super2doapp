<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{ asset('css/base.css') }}">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- CSS overrides - remove if you don't need it -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

</head>

<body>
    <section class="todoapp">
        {{-- Header  --}}
        @include('layouts.header')

        {{-- Content --}}
        @yield('container')

    </section>
    
    {{-- alert success --}}
    @if (session('status'))
    <div class="alert" id="message">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        {{ session('status') }}
    </div>
    @endif
    <!-- Scripts here. Don't remove ↓ -->
    <script src="{{ asset('js/app.js') }}"></script>

    {{-- script  changeStatus active and complete--}}
    <script>
        function changeStatus(_this, id) {
            var is_complete = $(_this).prop('checked') == true ? 1 : 0;
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                url: `/changestatus`,
                type: 'GET',
                data: {
                    _token: _token,
                    id: id,
                    is_complete: is_complete
                },
                success: function (result) {}
            });
        }
    </script>

    {{-- Hidden  alert success--}}
    <script>
        window.setTimeout(function () {
            $("#message").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 5000);
    </script>

</body>

</html>