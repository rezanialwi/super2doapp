        {{-- Header --}}
        <header class="header">
            <h1>Super2Do</h1>
            <div>
                <form method="POST" action="{{ route('tasks.store') }}">
                    @csrf
                    <input placeholder="What needs to be done?" id="title" name="title" type="text" maxlength="255"
                        class="new-todo form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                        autocomplete="off">
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </form>
            </div>
        </header>