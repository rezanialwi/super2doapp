# How to run Laravel Application
1. Setelah kita clone atau download dari repositori GitHub, atau dari manapun, jalankan perintah di command prompt, sebelumnya kita harus menuju ke lokasi folder aplikasi tersebut:
``` composer install ```
Perintah ini akan menginstal librari-librari atau dependencies yang digunakan Laravel.

2. Berikutnya, kita perlu membuat file .env berdasarkan dari file .env.example, caranya jalankan perintah:
```cp .env.example .env```

3. Setelah berhasil membuat file .env, beirkutnya jalankan perintah:
``` php artisan key:generate```

4. Berikutnya jalankan perintah berikut.
```php artisan migrate```


# How To Deploy Super2do App

### Langkah 1: Instalasi Laravel
Kita mulai dengan melakukan instalasi laravel. Silahkan gunakan salah satu perintah dibawah ini.
```
//composer
composer create-project laravel/laravel super2do

//laravel installer
laravel new super2do
```

### Langkah 2: Konfigurasi Database
Silahkan buat database MySQL baru kemudian buka file .env pada projek untuk dihubungkan dengan mengaturnya pada DB_DATABASE; seperti dibawah ini.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_super2do
DB_USERNAME=root
DB_PASSWORD=
```

### Langkah 3: Membuat File Migrasi, Model dan controller
Selanjutnya kita buat file migrasi baru untuk membuat tabel dan kolom database. Kita gunakan perintah dibawah ini untuk membuat file migrasi sekaligus membuat model untuk tabel yang dibuat.
```
php artisan make:model Task -rcm
```
File model yang terbuat berada di direktori app > Models dan file migrasi di direktori database > migrations.

### Langkah 4: Membuat Kolom Pada Migrasi yang kita buat
Kita buka file migrasi yang telah dibuat tadi untuk kita tambahkan beberapa kolom. Pada up() method silahkan buat seperti dibawah ini.
```
public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('is_complete');
            $table->timestamps();
        });
    }
```
Kemudian jalankan perintah ```php artisan migrate``` untuk mengirim ke database.

### Langkah 5 : Eloquent: Mass Assignment
Langkah selanjutnya kita buka model Task.php untuk membuat properti $fillable dan menambahkan kolom tabel, seperti dibawah ini.
```
class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';
    protected $fillable = [
        'title',
        'is_complete',
    ];
}
```

### Langkah 6 : Membuat Route
Selanjutnya kita buat route ke setiap method() di contoller menggunakan resources method.
Silahkan buka route web.php dan tambahkan dibawah ini.

```
Route::get('/', function () {
    return redirect('/tasks');
});

Route::get('tasks/active', [TaskController::class, 'active'])->name('active');
Route::get('tasks/completed', [TaskController::class, 'completed'])->name('completed');
Route::get('/changestatus', [TaskController::class, 'changeStatus'])->name('changestatus');
Route::get('/deleteall', [TaskController::class, 'deleteAll'])->name('deleteall');
Route::get('/completedall', [TaskController::class, 'completedAll'])->name('completedall');
Route::resource('tasks', TaskController::class)->only('index', 'store', 'destroy');
```
### Langkah 7 : Membuat Layouts dan atur controller
Setelah Model, Controller, Route telah dibuat, kita lanjutkan untuk mengerjakan fungsi membuat data atau meyimpan data ke database.
Pertama silahkan buat folder baru dengan nama layouts di direktori views dan buat sebuah file main.blade.php didalamnya template todo dan didalam folder layouts ada 2 file.
* header.blade.php
* main.blade.php

Selanjutnya pada index (), store(), destroy(), changeStatus() method di TaskController.php silahkan buat sepeti dibawah ini.
```
public function index()
    {
        $tasks = Task::
        orderBy('is_complete')
        ->orderByDesc('created_at')
        ->get();

        // return task index view with Task count
        return view('tasks', [
        'tasks' => $tasks,
        'tasks2' => Task::all()->count(),
        'tasks3' => Task::where('is_complete', '=', 1)->get()
        ]);
    }
```

Request simpan task
```
 public function store(Request $request)
    {
        // validate the given request
        $this->validate($request, [
            'title' => 'required|string|max:255',
        ]);

        // create a new incomplete task with the given title
        Task::create([
            'title' => $request->input('title'),
            'is_complete' => false,
        ]);

        // flash a success message to the session
        session()->flash('status', 'Task Created!');

        // redirect to tasks index
        return redirect('/tasks');
    }
```

Hapus record task
```
 public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        session()->flash('status', 'Task deleted success!');
        return redirect('/tasks');
    }
```
Metod ubah status active menjadi complete dan sebaliknya

```
 public function changeStatus(Request $request)
    {
        // mark the task as complete and save it
        $task = Task::find($request->id)->update(['is_complete' => $request->is_complete]);

        return response()->json(['success'=>'Status changed successfully.']);
    }
```
Metod hapus semua task completed

```
public function deleteAll()
    {
        Task::where('is_complete', '=', 1)->delete();  

        // flash a success message to the session
        session()->flash('status', 'Delete all Completed!');

        // redirect to tasks index
        return redirect('/tasks');
    }
```

Metod satuk kali klik mengubah status active menjadi complete untuk semu record/task
```
public function completedAll()
    {
        Task::query()->update(['is_complete' => 1]);

        // flash a success message to the session
        session()->flash('status', 'Completed all success!');

        // redirect to tasks index
        return redirect('/tasks');
    }
```

Lanjut atur tampilan tasks.blade di luar folder layouts.
Form input task otomatis tersimpan setelah tekan enter dan alert status success.
```
        <header class="header">
            <h1>Super2Do</h1>
            <div>
                <form method="POST" action="{{ route('tasks.store') }}">
                    @csrf
                    <input placeholder="What needs to be done?" id="title" name="title" type="text" maxlength="255"
                        class="new-todo form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                        autocomplete="off">
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </form>
            </div>
        </header>
```
Menampilkan list tasks
```
    <ul class="todo-list">
        @foreach ($tasks as $task)
        <li>
            <div class="view">

                @if ($task->is_complete)

                <input class="toggle" type="checkbox" onclick="changeStatus(event.target, {{ $task->id }});"
                    {{ $task->is_complete ? 'checked' : '' }}>
                <label>
                    <s>{{ $task->title }}</s>
                </label>
                @else
                <input class="toggle" type="checkbox" onclick="changeStatus(event.target, {{ $task->id }});"
                    {{ $task->is_complete ? 'checked' : '' }}>
                <label>
                    {{ $task->title }}
                </label>
                @endif
                {{-- Button delete taskss --}}
                <form action="{{ route('tasks.destroy', $task->id)}}" method="POST" class="d-inline">
                    @method('delete')
                    @csrf
                    <button class="destroy" onclick="return confirm('Are you sure you want to delete ?')">
                    </button>
                </form>
            </div>
        </li>
        @endforeach
    </ul>
```
### Membuat REST API
Buat API Resource
``` php artisan make:resource TaskResource ```
Untuk membuat atau generate resource class, kita dapat menggunakan perintah artisan make:resource seperti di atas. By default, resource akan ditempatkan di direktori app/Http/Resources dari aplikasi atau laravel project kita. API resource akan extend Illuminate\Http\Resources\Json\JsonResource class.
```
 public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'is_complete' => $this->is_complete,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
```

Buat Controller API
``` php artisan make:controller API/TaskController ```
Kemudian kita perlu membuat controller baru untuk membuat logic CRUD dengan REST API Sanctum. Jalankan perintah artisan seperti di atas untuk membuat file ProgramController.php di dalam direktori app/Http/Controllers/API.

Setelah berhasil generate file ProgramController.php, sekarang buka file controller tersebut dan ubah kode yang ada menjadi seperti kode di atas. Di file ProgramController.php ini, kita membuat method index, store, update, show dan destroy.

```
<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Task;
use App\Http\Resources\TaskResource;

class TaskController extends Controller
{
    public function index()
    {
        $data = Task::latest()->get();
        return response()->json([TaskResource::collection($data), 'Task fetched.']);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());       
        }

        $task = Task::create([
            'title' => $request->title,
            'is_complete' => false
         ]);
        
        return response()->json(['Task created successfully.', new TaskResource($task)]);
    }

    public function show($id)
    {
        $task = Task::find($id);
        if (is_null($task)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json([new TaskResource($task)]);
    }

    public function update(Request $request, Task $task)
    {
        $task->is_complete = true;
        $task->save();
        
        return response()->json(['Task updated successfully.', new TaskResource($task)]);
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return response()->json('Task deleted successfully');
    }

}
```
Penjelasan singkat dari keempat method tersebut:

*Index, method ini digunakan membuat logic untuk menampilkan semua data dari table programs.
*Store, method ini berfungsi untuk menambahkan data dari request ke table programs. Di method ini juga *dilengkapi dengan data validation.
*Show, berfungsi untuk menampilkan detail data program berdasarkan id.
*Update, berfungsi untuk menerima data request dan memperbarui data program (berdasarkan id) dengan data request yang baru.
*Delete, method ini berfungsi untuk menghapus data program berdasarkan id data yang dikirimkan.

Buat REST API Routes
```Route::resource('tasks', TaskController::class);```


### Langkah terakhir tinggal mencobanya.

``` php artisan serve ```

Testing API dengan Postman
waktunya untuk testing atau menguji REST API CRUD yang telah kita buat  di laravel 8. Pada pengujian ini, kita akan coba untuk create data, show data, update data, delete data dan menampilkan semua data program menggunakan Postman.

### Fetch All Data 
![fetch alll data](public/img/fetch-all-data.png)

### Create Data
![create data](public/img/create-data.png)

### Get Single Data
![get single data](public/img/get-single-data.png)

### Update Data
![update data](public/img/update-data.png)

### Delete Data
![delete data](public/img/delete-data.png)


## Interface
![delete data](public/img/interface.PNG)







