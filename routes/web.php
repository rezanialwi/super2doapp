<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/tasks');
});

Route::get('tasks/active', [TaskController::class, 'active'])->name('active');
Route::get('tasks/completed', [TaskController::class, 'completed'])->name('completed');
Route::get('/changestatus', [TaskController::class, 'changeStatus'])->name('changestatus');
Route::get('/deleteall', [TaskController::class, 'deleteAll'])->name('deleteall');
Route::get('/completedall', [TaskController::class, 'completedAll'])->name('completedall');
Route::resource('tasks', TaskController::class)->only('index', 'store', 'destroy');