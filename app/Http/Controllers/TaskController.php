<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::
        orderBy('is_complete')
        ->orderByDesc('created_at')
        ->get();

        // return task index view with Task count
        return view('tasks', [
        'tasks' => $tasks,
        'tasks2' => Task::all()->count(),
        'tasks3' => Task::where('is_complete', '=', 1)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the given request
        $this->validate($request, [
            'title' => 'required|string|max:255',
        ]);

        // create a new incomplete task with the given title
        Task::create([
            'title' => $request->input('title'),
            'is_complete' => false,
        ]);

        // flash a success message to the session
        session()->flash('status', 'Task Created!');

        // redirect to tasks index
        return redirect('/tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        session()->flash('status', 'Task deleted success!');
        return redirect('/tasks');
    }

    public function active()
    {
        $tasks = Task::where('is_complete', '=', 0)->
        orderBy('is_complete')
        ->orderByDesc('created_at')
        ->get();

        // return task index view with Task count
        return view('active', [
        'tasks' => $tasks,
        'tasks2' => Task::where('is_complete', '=', 0)->count(),
        'tasks3' => Task::where('is_complete', '=', 1)->get()
        ]);
    }

    public function completed()
    {
        $tasks = Task::where('is_complete', '=', 1)->
        orderBy('is_complete')
        ->orderByDesc('created_at')
        ->get();

        // return task index view with Task count
        return view('completed', [
        'tasks' => $tasks,
        'tasks2' => Task::where('is_complete', '=', 1)->count(),
        'tasks3' => Task::where('is_complete', '=', 1)->get()
        ]);
    }

    public function changeStatus(Request $request)
    {
        // mark the task as complete and save it
        $task = Task::find($request->id)->update(['is_complete' => $request->is_complete]);

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function deleteAll()
    {
        Task::where('is_complete', '=', 1)->delete();  

        // flash a success message to the session
        session()->flash('status', 'Delete all Completed!');

        // redirect to tasks index
        return redirect('/tasks');
    }

    public function completedAll()
    {
        Task::query()->update(['is_complete' => 1]);

        // flash a success message to the session
        session()->flash('status', 'Completed all success!');

        // redirect to tasks index
        return redirect('/tasks');
    }
}
