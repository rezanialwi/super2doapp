<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Task;
use App\Http\Resources\TaskResource;

class TaskController extends Controller
{
    public function index()
    {
        $data = Task::latest()->get();
        return response()->json([TaskResource::collection($data), 'Task fetched.']);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());       
        }

        $task = Task::create([
            'title' => $request->title,
            'is_complete' => false
         ]);
        
        return response()->json(['Task created successfully.', new TaskResource($task)]);
    }

    public function show($id)
    {
        $task = Task::find($id);
        if (is_null($task)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json([new TaskResource($task)]);
    }

    public function update(Request $request, Task $task)
    {
        $task->is_complete = true;
        $task->save();
        
        return response()->json(['Task updated successfully.', new TaskResource($task)]);
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return response()->json('Task deleted successfully');
    }

}
